/// <reference types="Cypress" />

describe('Testes de API', () => {
    it('Deve validar esquemas e status code', () => {
      cy.request({
        method: 'GET',
        url: '/api/users/2',
        headers: {
          // Adicione cabeçalhos necessários
        },
        // Adicione parâmetros ou corpo da requisição, se aplicável
      }).then((response) => {
        /*
        // Validação do esquema
        cy.wrap(response.body).should('deep.include', {
          // Esquema esperado
          
        });
        */
  
        // Validação do status code
        expect(response.status).to.equal(200);
      });
    });
  });
  